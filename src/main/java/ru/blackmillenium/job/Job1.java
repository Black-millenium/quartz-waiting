package ru.blackmillenium.job;

import org.apache.log4j.Logger;
import org.quartz.*;

public class Job1 implements Job {
    private static final Logger LOGGER = Logger.getLogger(Job1.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            LOGGER.info("Возникло исключение в задаче " + jobExecutionContext.getJobDetail().getKey().getName());
        }
    }
}
