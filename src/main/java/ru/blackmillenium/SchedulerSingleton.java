package ru.blackmillenium;

import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

public class SchedulerSingleton {
    private static final Logger LOGGER = Logger.getLogger(SchedulerSingleton.class);

    private static Scheduler scheduler;

    static {
        try {
            scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.start();
        } catch (SchedulerException e) {
            LOGGER.fatal("Невозможно запустить планировщик задач, приложение будет остановлено.");
            stopScheduler();
        }
    }

    private SchedulerSingleton() {
    }

    private static void stopScheduler() {
        try {
            scheduler.shutdown(false);
        } catch (SchedulerException e1) {
            LOGGER.fatal("Ошибка остановки планировщика. Приложение будет аварийно завершено.");
            System.exit(1);
        }
    }

    public static Scheduler getInstance() {
        return scheduler;
    }
}