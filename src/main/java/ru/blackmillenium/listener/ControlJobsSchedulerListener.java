package ru.blackmillenium.listener;

import org.apache.log4j.Logger;
import org.quartz.*;
import ru.blackmillenium.SchedulerSingleton;

public class ControlJobsSchedulerListener implements SchedulerListener {
    private static final Logger LOGGER = Logger.getLogger(ControlJobsSchedulerListener.class);

    private static final Object exitLock = new Object();
    private static int pendingJobs = 0;


    @Override
    public void schedulerShutdown() {
        LOGGER.info("Планировщик завершен");
        synchronized (exitLock) {
            exitLock.notify();  // уведомляем главный поток о завершении
        }
    }

    public static Object getExitLock() {
        LOGGER.info("ExitLock получен");
        return exitLock;
    }

    @Override
    public void triggerFinalized(Trigger trigger) {
        LOGGER.info("Уведомление о завершении задачи");
        if (--pendingJobs == 0)
            try {
                SchedulerSingleton.getInstance().shutdown();
            } catch (SchedulerException e) {
                e.printStackTrace();
            }

        LOGGER.info("Всего задач: " + pendingJobs);
    }

    @Override
    public void jobAdded(JobDetail jobDetail) {
        LOGGER.info("Задача добавлена");
        LOGGER.info("Всего задач: " + pendingJobs);
        ++pendingJobs;  // увеличиваем количество заданий
    }

    @Override
    public void jobScheduled(Trigger trigger) {

    }

    @Override
    public void jobUnscheduled(TriggerKey triggerKey) {

    }

    @Override
    public void triggerPaused(TriggerKey triggerKey) {

    }

    @Override
    public void triggersPaused(String s) {

    }

    @Override
    public void triggerResumed(TriggerKey triggerKey) {

    }

    @Override
    public void triggersResumed(String s) {

    }

    @Override
    public void jobDeleted(JobKey jobKey) {

    }

    @Override
    public void jobPaused(JobKey jobKey) {

    }

    @Override
    public void jobsPaused(String s) {

    }

    @Override
    public void jobResumed(JobKey jobKey) {

    }

    @Override
    public void jobsResumed(String s) {

    }

    @Override
    public void schedulerError(String s, SchedulerException e) {

    }

    @Override
    public void schedulerInStandbyMode() {

    }

    @Override
    public void schedulerStarted() {

    }

    @Override
    public void schedulerStarting() {

    }

    @Override
    public void schedulerShuttingdown() {

    }

    @Override
    public void schedulingDataCleared() {

    }
}
