package ru.blackmillenium;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import ru.blackmillenium.job.Job1;
import ru.blackmillenium.listener.ControlJobsSchedulerListener;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.JobKey.jobKey;

public class Run {
    public static void main(String[] args) {
        Scheduler instance = SchedulerSingleton.getInstance();
        ControlJobsSchedulerListener listener = new ControlJobsSchedulerListener();

        try {
            instance.getListenerManager().addSchedulerListener(listener);

            // создаем 1000 задач
            for (int i = 0; i < 1000; i++) {
                JobDetail jobDetail1 = newJob(Job1.class)
                        .withIdentity("Job " + i)
                        .storeDurably()
                        .build();

                instance.addJob(jobDetail1, false);
            }

            // запускаем 1000 задач
            for (int i = 0; i < 1000; i++) {
                instance.triggerJob(jobKey("Job " + i));
            }

            // ждем уведомления о завершении
            synchronized (ControlJobsSchedulerListener.getExitLock()) {
                ControlJobsSchedulerListener.getExitLock().wait();
            }

        } catch (SchedulerException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
